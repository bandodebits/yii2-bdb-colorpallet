Color Pallet
============
Color Pallet

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist bandodebits/yii2-bdb-colorpallet "*"
```

or add

```
"bandodebits/yii2-bdb-colorpallet": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \bdb\input\AutoloadExample::widget(); ?>```